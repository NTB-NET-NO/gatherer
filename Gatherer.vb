Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Imports System.Text
Imports Microsoft.Office.Interop
Imports Microsoft.Office.Core
Imports System.Text.RegularExpressions
Imports System.Xml
Imports System.Xml.Xsl
Imports System.IO
Imports System.Windows.Forms.Form




Public Class Gatherer
    Inherits System.ServiceProcess.ServiceBase
    Implements ccrpTimers6.ICcrpTimerNotify

#Region "Windows Form Designer generated code "
    Public Sub New()
        MyBase.New()
        If m_vb6FormDefInstance Is Nothing Then
            If m_InitializingDefInstance Then
                m_vb6FormDefInstance = Me
            Else
                Try
                    'For the start-up form, the first instance created is the default instance.
                    If System.Reflection.Assembly.GetExecutingAssembly.EntryPoint.DeclaringType Is Me.GetType Then
                        m_vb6FormDefInstance = Me
                    End If
                Catch
                End Try
            End If
        End If
        'This call is required by the Windows Form Designer.
        InitializeComponent()
    End Sub
    'Form overrides dispose to clean up the component list.
    Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
        If Disposing Then
            If Not components Is Nothing Then
                components.Dispose()
            End If
        End If
        MyBase.Dispose(Disposing)
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
#End Region
#Region "Upgrade Support "
    Private Shared m_vb6FormDefInstance As Gatherer
    Private Shared m_InitializingDefInstance As Boolean
    ' The main entry point for the process
    <MTAThread()> _
    Shared Sub Main()
        Dim ServicesToRun() As System.ServiceProcess.ServiceBase

        ' More than one NT Service may run within the same process. To add
        ' another service to this process, change the following line to
        ' create a second service object. For example,
        '
        '   ServicesToRun = New System.ServiceProcess.ServiceBase () {New Service1, New MySecondUserService}
        '
        ServicesToRun = New System.ServiceProcess.ServiceBase() {New Gatherer}

        System.ServiceProcess.ServiceBase.Run(ServicesToRun)
    End Sub

#End Region

    Dim Timer_Renamed As ccrpTimers6.ccrpTimer

    'MAPI session
    Dim mpsSession As MAPI.Session
    Dim isInfoStore As MAPI.InfoStore
    'root folder of infostore
    Dim fRoot As MAPI.Folder

    ' This is a change to show that we are to commit this file to SVN

    Public strRTFtekst As String
    Dim myErr As Short

    'Variables to hold INI-values
    'These values are assigned in GetIniValues() in this code
    '[Notifier] section
    Dim LOGFILE As String 'LogFile=
    Dim strInputDir As String 'InputDir =
    Dim strDoneDir As String 'DoneDir=
    Dim strTempDir As String 'TempDir=
    Dim strErrorDir As String 'ErrorDir=
    Dim nInterval As Integer 'Interval= (secs)
    Dim strXsltDoc As String

    '[Message] section
    Dim strFieldFileName As String 'MapFile=
    Dim strHeaderSep As String 'HeaderSep=

    '[Exchange] Section
    Dim strProfileName As String 'Profile=
    Dim strInfoStoreName As String 'InfoStore=
    Dim strDefaultFolder As String 'DefaultFolder=
    Dim strEnetpulseFolder As String
    Dim strInnenriksFolder As String
    Dim strUtenriksFolder As String
    Dim strSportsFolder As String
    Dim strPersFolder As String
    Dim strTipsFolder As String
    Dim strKulturFolder As String
    Dim strDivFolder As String
    Dim nRetryInterval As Integer 'RetryInterval=
    Dim strDefaultMessageType As String 'DefaultMessageType=
    Dim strMessageTypeRoot As String 'MessageTypeRoot=
    Dim bCreateFolder As Short

    'Headers written to the message that are not user defined
    Dim strSubjectHeader As String 'Header field containing subject
    Dim strFolderNameHeader As String 'Header field containing destination folders
    Dim strFileFormatHeader As String 'Header field containing file format
    Dim strPriFlagHeader As String 'Header field containing priority flag
    Dim strMessageTypeHeader As String 'Header field containing message type

    'Collection to hold legal header field types
    Dim coFormFields As Collection

    'Files that cannot be opened (may be locked)
    Dim coErrorFiles As New Collection

    'Folder objects
    Dim colFolderObjects As New Collection

    'Logger object
    Dim Logger As cLogger

    'Sequence number - used for temp files only
    Dim seqno As Short

    'String separating header from content
    Dim SEP As New VB6.FixedLengthString(1, ":")



    'Private objWord As New Word.ApplicationClass
    'Private objWordDoc As Word.DocumentClass

    Dim objWordAppl As New Microsoft.Office.Interop.Word.Application
    Dim objWordDoc As Microsoft.Office.Interop.Word.Document


    '    Private xsltWordML2Nitf As XslTransform
    '    Private xsltHead As XslTransform

    ' Private xslt4WordML2Nitf As MSXML2.IXSLProcessor  ' XSLT Problem workaround

    Public IPTC_sequence As Integer = 1
    '    Public strXmlMapFile As String
    '    Public writeDebugXml As Boolean
    '    Public deleteWordMlTempFile As Boolean


    Protected Overrides Sub OnStart(ByVal args() As String)
        'Date:          2006.05.23
        'Description:   Eventhandler for Load event


        On Error GoTo Err_Load

        Dim strDisplayName As String
        Dim bStarted As Boolean
        Dim iIniErr As Short
        Dim varFolderName As Object
        Dim iniFile As String

        Logger = New cLogger

        Timer_Renamed = New ccrpTimers6.ccrpTimer
        Timer_Renamed.Notify = Me

        iniFile = "C:\Gatherer\gatherer.ini"

        If CBool(LCase(CStr(VB.Left(VB.Command(), 2) = "-i"))) Then

            iniFile = Trim(VB.Right(VB.Command(), Len(VB.Command()) - 2))

        End If

        'Read ini file values
        iIniErr = GetIniValues(iniFile)
        Logger.Log(LOGFILE, "Load !s", "Settings read from " & iniFile)

        coFormFields = GetFormFields(strFieldFileName)
        Logger.Log(LOGFILE, "Load !s", "Headers read from " & strFieldFileName)

        'enable timer
        Timer_Renamed.Interval = nInterval
        Timer_Renamed.Enabled = True

        Exit Sub

Err_Load:
        Logger.Log(LOGFILE, "Load !x", Err.Description)
        End
    End Sub

    Protected Overrides Sub OnStop()
        ' Add code here to perform any tear-down necessary to stop your service.
        Timer_Renamed.Enabled = False
        objWordAppl.Quit()
        objWordAppl = Nothing
        Logger.Log(LOGFILE, "Service !s", "GATHERER service stopped.")
    End Sub
    Private Sub ICcrpTimerNotify_Timer(ByVal Milliseconds As Integer) Implements ccrpTimers6.ICcrpTimerNotify.Timer

        'disable timer when checking the directory
        Timer_Renamed.Enabled = False

        If fRoot Is Nothing Then
            If GetSession() = -1 Then
                Timer_Renamed.Interval = nRetryInterval
            Else
                Timer_Renamed.Interval = nInterval
            End If

        Else

            ProcessDir()

        End If

        Timer_Renamed.Enabled = True

    End Sub

    Public Sub ProcessDir()

        'Date:          1999.05.21
        'Description:   Processes all files in the notifier directory.



        Dim fiphdr As cFipHeader
        Dim creepCnt As Short
        Dim CurrentDirectory As String
        Dim newFile As cNewFile
        Dim LogMessage As String
        Dim strFF As String
        Dim bStatus As Boolean
        Dim fileLeftInQue As Boolean


        'disable timer - we process any leftovers now
        'Timer.Enabled = False

        'No need to start processing until we're logged on to exchange
        'If fRoot Is Nothing Then
        '    GetSession
        'End If

        'Check the directory...
        CurrentDirectory = Dir(strInputDir & "\*", FileAttribute.Normal)

        Do While CurrentDirectory <> ""

            'clear any errors from the last file
            myErr = 0

            newFile = New cNewFile
            newFile.FileName = strInputDir & "\" & CurrentDirectory

            Dim strkillfile As String
            strkillfile = CurrentDirectory

            'strkillfile = SubString(strkillfile, 2)
            strkillfile = strkillfile.Substring(0, 2)

            If strkillfile = "~$" Then
                Kill(newFile.FileName)
                Exit Do
            End If


            Dim strType As String

            strType = Microsoft.VisualBasic.Right(newFile.FileName, 3)
            strType = LCase(strType)
            Select Case strType
                Case "doc" ' check if dokument is of type Doc
                    ParseWordDoc(newFile)
                Case "xml"
                    ParseXMLDoc(newFile)
                    If myErr = 1 Then
                        Exit Sub
                    End If
                Case Else
                    ' MsgBox("Ikke Word dokument : " & " _ " & ("newFile.FileName"))
            End Select

            'Read fip headers and the text part
            myErr = ParseFile(newFile)
            'If myErr <> 0 Then
            '    'If file could not be opened - set flag for files left
            'fileLeftInQue = True

            'Else
            If myErr = 0 Then
                strFF = ""
                bStatus = True

                On Error Resume Next
                'check if file format is specified
                strFF = LCase(newFile.FipHeaders.Item(strFileFormatHeader).Content)

                'this is only for the log
                If strFF = "" Then strFF = "no format specified"

                'check file format
                Select Case strFF
                    Case "html" 'add HTML flag to body
                        newFile.BodyText = "{\fromhtml1" & vbCrLf & newFile.BodyText & "}"
                    Case "text" 'add plain text flag to body
                        newFile.PlainText = True
                        myErr = ConvertToRTF(newFile)
                    Case "rtf"
                        ''   'do nothing since the body text is already RTF
                    Case Else 'convert with word
                        '  myErr = ConvertToRTF(newFile)
                        'do nothing since the body text hopefully already are in RTF
                End Select

                'move to error que if convert failed
                If myErr <> 0 Then
                    MoveToErrorDir((newFile.FileName))
                Else
                    'Post message to exchange folders
                    myErr = PostMessage(newFile)

                    'error 1 
                    If myErr = 1 Then
                        MoveToErrorDir((newFile.FileName))
                        Logger.Log(LOGFILE, "FEIL ", "Fil flyttet til ErrorDir -> " & newFile.FileName)

                        'error 333 = folder not found -> move to error que
                    ElseIf myErr = 333 Then
                        MoveToErrorDir((newFile.FileName))
                        Logger.Log(LOGFILE, "FEIL ", "Fil flyttet til ErrorDir -> " & newFile.FileName)

                        'error 666 -> not logged on exit to timer
                    ElseIf myErr = 666 Then
                         newFile = Nothing
                        'Timer.Interval = nRetryInterval
                        Exit Sub

                        'move to done que if no error
                    ElseIf myErr = 0 Then
                        On Error Resume Next
                        'move to done folder
                        If strDoneDir <> "" Then
                            FileCopy(newFile.FileName, strDoneDir & "\" & GetBaseName((newFile.FileName)))
                            'delete input file
                            Kill(newFile.FileName)
                            If Err.Number <> 0 Then
                                Logger.Log(LOGFILE, "ProcessDir !x", "** Error removing " & newFile.FileName & ": " & Err.Description)
                            End If 'err on kill file
                        End If

                    End If ' err on post

                    End If 'err on convert file

            End If 'err on ParseFile

            'empty object - ready for next
            newFile = Nothing

            'read next item
            CurrentDirectory = Dir()

        Loop

        Timer_Renamed.Interval = nInterval

    End Sub

    Private Sub MoveToErrorDir(ByRef FileName As String)
        'Moves a file to the error que and logs

        On Error Resume Next
        FileCopy(FileName, strErrorDir & "\" & GetBaseName(FileName))
        If Err.Number = 0 Then
            Logger.Log(LOGFILE, "ProcessDir !x", "** " & GetBaseName(FileName) & " moved to error que **")
            Kill(FileName)
        End If
    End Sub

    Private Function GetIniValues(ByRef iniFile As String) As Short
        On Error GoTo GetIniError

        '***[Notifier]

        'get logfile location. NB! Read this value first from ini file, or nothing will be logged
        LOGFILE = gfnGetFromIni(iniFile, "WinNT", "LogFile", "c:\gatherer.log")
        Kill(LOGFILE)
        Logger.Log(LOGFILE, "GetIniValues", "Logging started. Logfile = " & LOGFILE)

        'get in-directory where files are dumped by FingerPost
        strInputDir = gfnGetFromIni(iniFile, "WinNT", "inputdir", "")
        Logger.Log(LOGFILE, "GetIniValues", "inputdir = " & strInputDir)


        'get XSLT doc - to transform XML files into Notabene
        strXsltDoc = gfnGetFromIni(iniFile, "XML", "XSLTDoc", "")
        Logger.Log(LOGFILE, "GetIniValues", "XSLTDoc = " & strXsltDoc)

        'get done dir
        strDoneDir = gfnGetFromIni(iniFile, "WinNT", "donedir", "")
        Logger.Log(LOGFILE, "GetIniValues", "donedir = " & strDoneDir)

        'error folder
        strErrorDir = gfnGetFromIni(iniFile, "WinNT", "errordir", "")
        Logger.Log(LOGFILE, "GetIniValues", "errordir = " & strErrorDir)

        'get temp dir
        strTempDir = gfnGetFromIni(iniFile, "WinNT", "tempdir", "c:\temp")
        Logger.Log(LOGFILE, "GetIniValues", "tempdir = " & strTempDir)

        'get notifier interval
        nInterval = CInt(Val(gfnGetFromIni(iniFile, "WinNT", "Interval", "1"))) * 1000
        Logger.Log(LOGFILE, "GetIniValues", "Interval = " & nInterval)

        '***[Message]

        'get path to fields cfg file from ini file
        strFieldFileName = gfnGetFromIni(iniFile, "Message", "MapFile", "")
        Logger.Log(LOGFILE, "GetIniValues", "MapFile = " & strFieldFileName)

        'get header separator
        strHeaderSep = gfnGetFromIni(iniFile, "Message", "HeaderSep", "~")
        Logger.Log(LOGFILE, "GetIniValues", "HeaderSep = " & strHeaderSep)

        '***[Exchange]

        'get profile name
        strProfileName = gfnGetFromIni(iniFile, "Exchange", "Profile", "")
        Logger.Log(LOGFILE, "GetIniValues", "Profile = " & strProfileName)

        'get infostore name from section "Exchange" of ini file, default value = "Public folders"
        strInfoStoreName = gfnGetFromIni(iniFile, "Exchange", "InfoStore", "")
        Logger.Log(LOGFILE, "GetIniValues", "InfoStore = " & strInfoStoreName)

        'get default folder
        strDefaultFolder = gfnGetFromIni(iniFile, "Exchange", "DefaultFolder", "Junk")
        Logger.Log(LOGFILE, "GetIniValues", "DefaultFolder = " & strDefaultFolder)

        'get Innenriks folder
        strEnetpulseFolder = gfnGetFromIni(iniFile, "Exchange", "EnetpulseFolder", "")
        Logger.Log(LOGFILE, "GetIniValues", "EnetpulseFolder = " & strEnetpulseFolder)

        'get Innenriks folder
        strInnenriksFolder = gfnGetFromIni(iniFile, "Exchange", "InnenriksFolder", "")
        Logger.Log(LOGFILE, "GetIniValues", "InnenriksFolder = " & strInnenriksFolder)

        'get Innenriks folder
        strUtenriksFolder = gfnGetFromIni(iniFile, "Exchange", "UtenriksFolder", "")
        Logger.Log(LOGFILE, "GetIniValues", "UtenriksFolder = " & strUtenriksFolder)

        'get Innenriks folder
        strSportsFolder = gfnGetFromIni(iniFile, "Exchange", "SportsFolder", "")
        Logger.Log(LOGFILE, "GetIniValues", "SportsFolder = " & strSportsFolder)

        'get KulturFolder
        strKulturFolder = gfnGetFromIni(iniFile, "Exchange", "KulturFolder", "")
        Logger.Log(LOGFILE, "GetIniValues", "KulturFolder = " & strKulturFolder)

        'get Persfolder
        strPersFolder = gfnGetFromIni(iniFile, "Exchange", "PersFolder", "")
        Logger.Log(LOGFILE, "GetIniValues", "TipsFolder = " & strPersFolder)

        'get Tipsfolder
        strTipsFolder = gfnGetFromIni(iniFile, "Exchange", "TipsFolder", "")
        Logger.Log(LOGFILE, "GetIniValues", "KulturFolder = " & strTipsFolder)


        'get Diverse folder
        strDivFolder = gfnGetFromIni(iniFile, "Exchange", "DivFolder", "")
        Logger.Log(LOGFILE, "GetIniValues", "DivFolder = " & strDivFolder)

        'get message type
        strDefaultMessageType = gfnGetFromIni(iniFile, "Exchange", "defaultMessageType", "")
        Logger.Log(LOGFILE, "GetIniValues", "DefaultMessageType = " & strDefaultMessageType)

        'get root of message type
        strMessageTypeRoot = gfnGetFromIni(iniFile, "Exchange", "MessageTypeRoot", "IPM.Note.")
        Logger.Log(LOGFILE, "GetIniValues", "MessageTypeRoot = " & strMessageTypeRoot)

        'get retry interval
        nRetryInterval = CInt(Val(gfnGetFromIni(iniFile, "Exchange", "RetryInterval", "10"))) * 1000
        Logger.Log(LOGFILE, "GetIniValues", "RetryInterval = " & nRetryInterval)

        bCreateFolder = CShort(Val(gfnGetFromIni(iniFile, "Exchange", "CreateFolder", "0")))
        Logger.Log(LOGFILE, "GetIniValues", "CreateFolder = " & bCreateFolder)

        Exit Function

GetIniError:
        Logger.Log(LOGFILE, "GetIniValues", Err.Number & " " & Err.Description)

    End Function

    Private Function GetFormFields(ByRef strFilename As String) As Collection

        'Reads the map file to find which header fields that maps to
        'the Outlook fields.

        Dim textLine As Object
        Dim i As Object
        Dim j As Short
        Dim colReturnCollection As Collection
        Dim thisFormField As cFormField
        Dim filehandle As Short
        Dim fiphdr As Object
        Dim formfld As String
        Dim strParts As Object

        filehandle = FreeFile()

        colReturnCollection = New Collection

        FileOpen(filehandle, strFilename, OpenMode.Input)

        Do While Not EOF(1)
            textLine = LineInput(filehandle)
            If VB.Left(LTrim(textLine), 1) <> ";" Then
                strParts = Split(textLine, ":")
                If UBound(strParts) > 0 Then
                    fiphdr = strParts(1)

                    Select Case LCase(strParts(0))
                        Case "subject"
                            strSubjectHeader = Trim(strParts(1))
                            Logger.Log(LOGFILE, "GetFormFields !s", "SubjectHeader: " & strSubjectHeader)

                        Case "foldername"
                            strFolderNameHeader = Trim(strParts(1))
                            Logger.Log(LOGFILE, "GetFormFields !s", "FoldeNameHeader: " & strFolderNameHeader)

                        Case "fileformat"
                            strFileFormatHeader = Trim(strParts(1))
                            Logger.Log(LOGFILE, "GetFormFields !s", "FileFormatHeader: " & strFileFormatHeader)

                        Case "priority-flag"
                            strPriFlagHeader = Trim(strParts(1))
                            Logger.Log(LOGFILE, "GetFormFields !s", "PriFlagHeader: " & strPriFlagHeader)

                        Case "message-type"
                            strMessageTypeHeader = Trim(strParts(1))
                            Logger.Log(LOGFILE, "GetFormFields !s", "MessageTypeHeader: " & strMessageTypeHeader)

                        Case Else
                            formfld = Trim(strParts(0))
                    End Select

                    If fiphdr <> "" Then
                        thisFormField = New cFormField
                        thisFormField.FipHeader = fiphdr
                        thisFormField.FormField = formfld

                        If UBound(strParts) = 2 Then
                            Select Case Trim(LCase(strParts(2)))
                                Case "integer"
                                    thisFormField.FieldType = VariantType.Short
                                Case "date"
                                    thisFormField.FieldType = VariantType.Date
                                Case "long"
                                    thisFormField.FieldType = VariantType.Integer
                                Case "double"
                                    thisFormField.FieldType = VariantType.Double
                                Case Else
                                    thisFormField.FieldType = VariantType.String
                            End Select
                        Else
                            thisFormField.FieldType = VariantType.String
                        End If

                        colReturnCollection.Add(thisFormField, thisFormField.FipHeader)

                        If formfld <> "" Then Logger.Log(LOGFILE, "GetFormFields !s", formfld & ": " & fiphdr & ": " & thisFormField.FieldType)
                        thisFormField = Nothing
                    End If

                End If
            End If
        Loop

        FileClose(filehandle)

        GetFormFields = colReturnCollection
        colReturnCollection = Nothing

    End Function


    Private Function ParseWordHeader(ByVal strName As String, ByRef newFile As cNewFile) As Short
        Dim fh As Short
        Dim status As String
        Dim strFiphdr As Object
        Dim strContent As String
        Dim thisHdr As cFipHeader
        Dim thisFormFld As cFormField
        Dim errorFile As cErrorFile
        Dim i As Short
        Dim openCount As Short
        Dim textline As String

        fh = FreeFile()

        Err.Clear()

        textline = strName

        'open input file
        'Open newFile.FileName For Input Lock Write As #fh
        FileOpen(fh, newFile.FileName, OpenMode.Binary, , OpenShare.LockReadWrite)
        i = InStr(1, textline, ":")
        If (i > 0) Then
            'Found a Separator, read header
            strFiphdr = VB.Left(textline, i - 1)
            strContent = VB.Right(textline, Len(textline) - i)

            If strFiphdr <> "" And strContent <> "" Then

                'Check if this header is already read
                On Error Resume Next

                thisHdr = newFile.FipHeaders.Item(strFiphdr)
                On Error GoTo 0

                If Not thisHdr Is Nothing Then
                    'it's already there, overwrite
                    thisHdr.Content = strContent

                Else
                    'check if we want this header
                    On Error Resume Next
                    thisFormFld = coFormFields.Item(strFiphdr)
                    On Error GoTo 0

                    If Not thisFormFld Is Nothing Then
                        'we want it, add new header object
                        thisHdr = New cFipHeader
                        thisHdr.HeaderName = strFiphdr
                        thisHdr.Content = strContent
                        newFile.FipHeaders.Add(thisHdr, strFiphdr)

                        thisHdr = Nothing
                        thisFormFld = Nothing
                    End If
                End If
            End If
            strFiphdr = ""
            strContent = ""
        End If


    End Function

    Private Function ParseXMLDoc(ByRef newFile As cNewFile) As Short

        Dim oDocumentProperty 'As Word.CustomProperty
        Dim tempRTF As String
        Dim CurrentDirectory As String

        'Logger.Log(LOGFILE, "Word skal lese -> ", (newFile.FileName))

        If File.Exists(newFile.FileName) Then

            Sleep(3000)

            'Logger.Log(LOGFILE, "Fant f�lgende fil -> ", (newFile.FileName))
            'get an instance of word
            objWordAppl = gGetWordApplication()

            Try
                objWordAppl.Documents.Open(newFile.FileName, XMLTransform:=strXsltDoc)
            Catch ex As Exception


                'objWordAppl.Quit()
                'objWordAppl = Nothing
                Logger.Log(LOGFILE, "Feiler i �pning av Word dok. - Feil nr. ", ex.ToString)
                MoveToErrorDir((newFile.FileName))
                myErr = 1
                Logger.Log(LOGFILE, "Feilet skjekker om nye filer -> ", "Til ProcessDir")
                ProcessDir()
                Exit Function
            End Try

        Else
            Logger.Log(LOGFILE, "Feiler i om filen eksisterer -> ", "g�r derfor til ProcessDir")
            Call ProcessDir()
            Exit Function
        End If



        'Skal bebynne � forbedrede RTF for � f� dette inn i Exchange

        seqno = seqno + 1
        If seqno > 9999 Then seqno = 1

        tempRTF = strTempDir & "\" & "strInputTempFile" & "-" & Format(objWordAppl.Name & "-" & seqno, "0000") & ".rtf"

        Try
            objWordAppl.ActiveDocument.SaveAs(tempRTF, Word.WdSaveFormat.wdFormatRTF)
        Catch ex As Exception
            Logger.Log(LOGFILE, "Feiler i lagring av RTF ", ex.ToString)
            myErr = 1
            newFile.FileName = tempRTF
            MoveToErrorDir((newFile.FileName))
            Call ProcessDir()
            Exit Function
        End Try



        Try
            Dim strFolderName As String
            Dim strMeldingsSign As String
            Dim strContent As String
            Dim strName As String
            Dim strHeader
            Dim textline As String
            Dim QD As String

            Dim strTemp As String
            Dim intFolderUsed As Boolean

            intFolderUsed = False

            For Each oDocumentProperty In objWordAppl.ActiveDocument.CustomDocumentProperties
                strContent = oDocumentProperty.Value
                strName = oDocumentProperty.Name

                If strName = "foldername" And intFolderUsed = True Then
                    strContent = ""
                    strName = ""
                End If

                If strName = "NTBMeldingsSign" Then
                    strTemp = strContent
                    Select Case LCase(strTemp)
                        Case "enetpulse" ' innenriks 
                            If strEnetpulseFolder <> "" Then
                                strHeader = strHeader & "foldername" & ":" & strEnetpulseFolder & vbCrLf
                                intFolderUsed = True
                            Else
                                Logger.Log(LOGFILE, "NTBMeldingsSign skal v�re : ", strName & " - " & strContent)
                            End If
                    End Select
                End If


                If strName = "NTBStoffgruppe" And intFolderUsed = False Then

                    strTemp = strContent
                    strTemp = Mid(strTemp, 1, 2)
                    intFolderUsed = True

                    Select Case LCase(strTemp)
                        Case "in" ' innenriks 
                            If strInnenriksFolder <> "" Then
                                strHeader = strHeader & "foldername" & ":" & strInnenriksFolder & vbCrLf
                            Else
                                Logger.Log(LOGFILE, "NTBStoffgruppe skal v�re : ", strName & " - " & strContent)
                            End If

                        Case "ut" ' utenriks 
                            If strUtenriksFolder <> "" Then
                                strHeader = strHeader & "foldername" & ":" & strUtenriksFolder & vbCrLf
                            Else
                                Logger.Log(LOGFILE, "NTBStoffgruppe skal v�re : ", strName & " - " & strContent)
                            End If

                        Case "sp" ' sport 
                            If strSportsFolder <> "" Then
                                strHeader = strHeader & "foldername" & ":" & strSportsFolder & vbCrLf
                            Else
                                Logger.Log(LOGFILE, "NTBStoffgruppe skal v�re : ", strName & " - " & strContent)
                            End If

                        Case "ku", "sh" ' Kultur 
                            If strKulturFolder <> "" Then
                                strHeader = strHeader & "foldername" & ":" & strKulturFolder & vbCrLf
                            Else
                                Logger.Log(LOGFILE, "NTBStoffgruppe skal v�re : ", strName & " - " & strContent)
                            End If

                        Case "da", "ju" ' dagen i dag og jubilant 
                            If strPersFolder <> "" Then
                                strHeader = strHeader & "foldername" & ":" & strPersFolder & vbCrLf
                            Else
                                Logger.Log(LOGFILE, "NTBStoffgruppe skal v�re : ", strName & " - " & strContent)
                            End If

                        Case "v5", "ti", "la", "dd" ' v5 / dd /langodds 
                            If strTipsFolder <> "" Then
                                strHeader = strHeader & "foldername" & ":" & strTipsFolder & vbCrLf
                            Else
                                Logger.Log(LOGFILE, "NTBStoffgruppe skal v�re : ", strName & " - " & strContent)
                            End If

                        Case Else
                            If strDivFolder <> "" Then
                                strHeader = strHeader & "foldername" & ":" & strDivFolder & vbCrLf
                            Else
                                Logger.Log(LOGFILE, "NTBStoffgruppe skal v�re noe annet: ", strName & " - " & strContent)
                            End If

                            strTemp = ""
                    End Select
                End If

                strHeader = strHeader & strName & ":" & strContent & vbCrLf
            Next

            objWordAppl.ActiveDocument.Close(False)


            If objWordAppl.Documents.Count > 0 Then
                Dim i As Integer
                ' MsgBox(objWordAppl.Documents.Count)
                i = objWordAppl.Documents.Count
                For i = i To 1 Step -1
                    objWordAppl.ActiveDocument.Close()
                    Sleep(2000)
                Next
            End If

            Dim fh As Short
            fh = FreeFile()


            Dim sr As StreamReader = File.OpenText(tempRTF)

            strRTFtekst = sr.ReadToEnd()
            sr.Close()


            FileOpen(fh, tempRTF, OpenMode.Output, , OpenShare.LockReadWrite)
            PrintLine(fh, "~")
            PrintLine(fh, strHeader)
            PrintLine(fh, "~")
            PrintLine(fh, strRTFtekst)

            'close file
            FileClose(fh)

            If fh = 1 Then
                FileClose(fh)
            End If

        Catch ex As Exception
            Logger.Log(LOGFILE, "Fors�kte � lage fip header ", " Feilmelding er: " & Err.Description)
        End Try


        'move to done folder
        If strDoneDir <> "" Then
            FileCopy(newFile.FileName, strDoneDir & "\" & GetBaseName((newFile.FileName)))
            'Kill(tempXML)
            Kill(newFile.FileName)
            If Err.Number <> 0 Then
                Logger.Log(LOGFILE, "ProcessDir !x", "** Error removing " & newFile.FileName & ": " & Err.Description)
            End If 'err on kill file
        End If

        newFile.FileName = tempRTF
        tempRTF = ""


        If Err.Number <> 0 Then
            Logger.Log(LOGFILE, "Ferdig i ParseXMLDoc fant feil -> Err.number " & Err.Number)
            Err.Clear()
            myErr = 1
        End If

        Exit Function

    End Function


    Private Function ParseWordDoc(ByRef newFile As cNewFile) As Short

        Dim oDocumentProperty 'As Word.CustomProperty
        Dim strBody
        Dim tempRTF As String

        On Error GoTo err_convert

        'get an instance of word
        objWordAppl = gGetWordApplication()


        'Open temp file in Word
        objWordAppl.Documents.Open(newFile.FileName)


        seqno = seqno + 1
        If seqno > 9999 Then seqno = 1

        tempRTF = strTempDir & "\" & "strInputTempFile" & "-" & Format(objWordAppl.Name & "-" & seqno, "0000") & ".rtf"
        objWordAppl.ActiveDocument.SaveAs(tempRTF, Word.WdSaveFormat.wdFormatRTF)


        Dim strContent
        Dim strName
        Dim strHeader
        Dim textline As String
        For Each oDocumentProperty In objWordAppl.ActiveDocument.CustomDocumentProperties
            strContent = oDocumentProperty.Value
            strName = oDocumentProperty.Name
            strHeader = strHeader & strName & ":" & strContent & vbCrLf
        Next


        objWordAppl.ActiveDocument.Close()

        Dim strNTBID As String
        strNTBID = Format(Now, "yyyyddmm_hhmmss")
        strNTBID = "RED_" & strNTBID & "_RU_00"
        strHeader = strHeader & "NTBID" & ":" & strNTBID & vbCrLf
        strHeader = strHeader & "NTBBrukerSign" & ":RedUser" & vbCrLf


        ' MsgBox(objWordAppl.Documents.Count)


        If objWordAppl.Documents.Count > 0 Then
            Dim i As Integer
            ' MsgBox(objWordAppl.Documents.Count)
            i = objWordAppl.Documents.Count
            For i = i To 1 Step -1
                objWordAppl.ActiveDocument.Close()
                Sleep(2000)
            Next
        End If

        Dim fh As Short
        fh = FreeFile()

        FileOpen(fh, tempRTF, OpenMode.Input)
        Do While Not EOF(fh)
            textline = LineInput(fh)
            strBody = strBody & textline
            strRTFtekst = strBody
        Loop

        Sleep(500)

        FileClose(fh)

        FileOpen(fh, tempRTF, OpenMode.Output, , OpenShare.LockReadWrite)
        PrintLine(fh, "~")
        PrintLine(fh, strHeader)
        PrintLine(fh, "~")
        PrintLine(fh, strBody)

        Sleep(500)
        'close file
        FileClose(fh)

        If fh = 1 Then
            FileClose(fh)
        End If




        'move to done folder
        If strDoneDir <> "" Then
            FileCopy(newFile.FileName, strDoneDir & "\" & GetBaseName((newFile.FileName)))
            'Kill(tempXML)
            Kill(newFile.FileName)
            If Err.Number <> 0 Then
                Logger.Log(LOGFILE, "ProcessDir !x", "** Error removing " & newFile.FileName & ": " & Err.Description)
            End If 'err on kill file
        End If

        newFile.FileName = tempRTF
        strRTFtekst = strBody
        tempRTF = ""

        Err.Clear()

        Exit Function

err_convert:
        FileClose(fh)
        On Error GoTo 0
        Err.Clear()
        Kill(tempRTF)
    End Function


    Private Function ParseFile(ByRef newFile As cNewFile) As Short

        'Parse the file
        'Reads the Fip header (if any) and adds the text to a variable
        'Returns 0 on success


        Dim fh As Short
        Dim status As String
        Dim strFiphdr As Object
        Dim strContent As String
        Dim thisHdr As cFipHeader
        Dim thisFormFld As cFormField
        Dim errorFile As cErrorFile
        Dim textLine As String
        Dim i As Short
        Dim openCount As Short
        Dim strFF As String

        fh = FreeFile()

        On Error Resume Next

        'check if this is already in the error collection
        errorFile = coErrorFiles.Item(newFile.FileName)
        If Err.Number = 0 And errorFile.Count >= 5 Then
            If DateDiff(Microsoft.VisualBasic.DateInterval.Minute, errorFile.LastTry, Now) >= 1 Then
                errorFile.Count = 4
            Else
                ParseFile = 11
                Exit Function
            End If
        End If

        Err.Clear()

        'open input file
        'Open newFile.FileName For Input Lock Write As #fh
        FileOpen(fh, newFile.FileName, OpenMode.Binary, , OpenShare.LockReadWrite)


        If Err.Number = 0 Then
            If Not errorFile Is Nothing Then
                coErrorFiles.Remove(errorFile.FileName)
                errorFile = Nothing
            End If

            'if error - add to error collection
        Else
            If errorFile Is Nothing Then
                errorFile = New cErrorFile
                errorFile.FileName = newFile.FileName
                errorFile.Count = 1
                coErrorFiles.Add(errorFile, errorFile.FileName)
            Else
                errorFile.Count = errorFile.Count + 1
            End If

            If errorFile.Count = 5 Then
                errorFile.LastTry = Now
                Logger.Log(LOGFILE, "ParseFile !x", "Could not lock " & newFile.FileName & " (retrying in 1 min)")
            Else
                Logger.Log(LOGFILE, "ParseFile !x", "Could not lock " & newFile.FileName & " (retrying in " & CShort(nInterval / 1000) & " secs)")
            End If

            errorFile = Nothing

            ParseFile = 12
            Exit Function

        End If

        status = "beforeSOH"
        '-- Read input file
        Do While Not EOF(fh)
            textLine = LineInput(fh)

            'check where we are at the moment
            Select Case status
                Case "beforeSOH" 'expecting line to begin with SOH character
                    If Trim(textLine) = strHeaderSep Then
                        'found headersep - ready for the header
                        status = "header"
                    Else
                        'no header found on the first line - expecting all body
                        newFile.BodyText = textLine & vbCrLf
                        status = "body"
                    End If

                Case "header" 'in the header, expect field:value, blank line or headersep
                    If Trim(textLine) = "" Then
                        'do nothing - blank line in header

                    ElseIf Trim(textLine) = strHeaderSep Then
                        'End of Header..
                        status = "body"

                    Else
                        'still in header - expect a field (name:value)
                        'i = InStr(1, textLine, SEP)
                        i = InStr(1, textLine, ":")
                        If (i > 0) Then
                            'Found a Separator, read header
                            strFiphdr = VB.Left(textLine, i - 1)
                            strContent = VB.Right(textLine, Len(textLine) - i)

                            If strFiphdr <> "" And strContent <> "" Then

                                'Check if this header is already read
                                On Error Resume Next
                                thisHdr = newFile.FipHeaders.Item(strFiphdr)
                                On Error GoTo 0

                                If Not thisHdr Is Nothing Then
                                    'it's already there, overwrite
                                    thisHdr.Content = strContent

                                Else
                                    'check if we want this header
                                    On Error Resume Next
                                    thisFormFld = coFormFields.Item(strFiphdr)
                                    On Error GoTo 0

                                    If Not thisFormFld Is Nothing Then
                                        'we want it, add new header object
                                        thisHdr = New cFipHeader
                                        thisHdr.HeaderName = strFiphdr
                                        ' This test for to handle Doc and XML files - some of them crash.
                                        If strFiphdr = "QF" Then
                                            strFF = strContent
                                        End If
                                        thisHdr.Content = strContent
                                        newFile.FipHeaders.Add(thisHdr, strFiphdr)

                                        ''Logger.Log LOGFILE, "ParseFile !d", textLine
                                        ''Logger.Log LOGFILE, "ParseFile !d", thisHdr.HeaderName & "=" & thisHdr.Content

                                        thisHdr = Nothing
                                        'UPGRADE_NOTE: Object thisFormFld may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1029"'
                                        thisFormFld = Nothing
                                    End If
                                End If
                            End If
                            strFiphdr = ""
                            strContent = ""
                        End If
                    End If

                Case "body" 'add current line to body, then exit loop
                    newFile.BodyText = newFile.BodyText & textLine & vbCrLf
                    Exit Do

                Case Else
                    'oops.. illegal status. report error and abort!
                    FileClose(fh)
                    Logger.Log(LOGFILE, "ParseFile", "Illegal parse status: " & status)
                    ParseFile = 1
                    Exit Function

            End Select

        Loop

        If strRTFtekst <> "" Then
            newFile.BodyText = strRTFtekst
        Else
            Do While Not EOF(fh)
                textLine = LineInput(fh)
                newFile.BodyText = newFile.BodyText & textLine
            Loop
        End If

        'Rydder opp
        FileClose(fh)
        ParseFile = 0
        Exit Function
    End Function

    Private Function ConvertToRTF(ByRef newFile As cNewFile) As Short

        'Convert the body of the file to RTF using word.
        'This may not succeed.
        'Returns a non zero value on serious errors


        Dim fh As Short
        Dim tempRTF As Object
        Dim tempXML As String
        Dim textLine As String
        Dim strBody As String
        Dim wordapplication As Microsoft.Office.Interop.Word.Application

        fh = FreeFile()

        'set up temp names
        seqno = seqno + 1
        If seqno > 9999 Then seqno = 1
        tempRTF = strTempDir & "\gatherer" & Format(seqno, "0000") & ".rtf"
        tempXML = strTempDir & "\gatherer" & Format(seqno, "0000") & ".html"

        On Error GoTo err_convert

        'get an instance of word
        wordapplication = gGetWordApplication()

        'Print body text to temp file
        FileOpen(fh, tempXML, OpenMode.Output) 'open file
        PrintLine(fh, newFile.BodyText)
        FileClose(fh)

        'Open temp file in Word
        wordapplication.Documents.Open(tempXML, False)

        'save as rtf and close
        wordapplication.ActiveDocument.SaveAs(tempRTF, Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatRTF)
        wordapplication.ActiveDocument.Close()

        'read the rtf text into newfile.bodytext
        FileOpen(fh, tempRTF, OpenMode.Input)
        Do While Not EOF(fh)
            textLine = LineInput(fh)
            strBody = strBody & textLine
            strRTFtekst = strBody
        Loop
        FileClose(fh)
        newFile.BodyText = strBody

        'zap temp files
        Kill(tempXML)
        Kill(tempRTF)

        wordapplication = Nothing

        ConvertToRTF = 0
        Exit Function

err_convert:
        Logger.Log(LOGFILE, "ConvertToRTF !x", "** " & Err.Description & " **")
        ConvertToRTF = Err.Number
    End Function

    Private Function PostMessage(ByRef newFile As cNewFile) As Short

        'Posting a message to an Exchange Public Folder
        'File is already parsed in ParseFile and the BodyText is set
        'to something Exchange accepts (RTF, HTML or plain text)


        Dim bRet As Short
        Dim folderCount As Short
        Dim msgID As String
        Dim strFolders As String
        Dim thisVal As String
        Dim formfld As cFormField
        Dim coDestFolders As Collection
        Dim thisFolder As MAPI.Folder
        Dim msgFields As MAPI.Fields
        Dim aMessageField As MAPI.Field
        Dim orgMessage As MAPI.Message
        Dim cpyMessage As MAPI.Message

        'get the root folder if we're not logged on yet
        If fRoot Is Nothing Then
            If GetSession() <> 0 Then
                'Sleep nRetryInterval
                PostMessage = 666
                Exit Function
            End If
        End If

        On Error Resume Next
        'get string containing destination folders
        strFolders = newFile.FipHeaders.Item(strFolderNameHeader).Content

        'set to default if no folder is specified
        If strFolders = "" Then
            Logger.Log(LOGFILE, "Message -> ", newFile.FipHeaders.Item(strSubjectHeader).Content & " - " & strFolderNameHeader & " - Save to default folder **")
            strFolders = strDefaultFolder
        End If

        On Error GoTo post_err

        'get pointers to exchange folders
        coDestFolders = ExtractFolderList(strFolders, fRoot)
        'report error and exit if no folder was found (not even the default)
        If coDestFolders.Count() = 0 Then
            Logger.Log(LOGFILE, "PostMessage !x", "** " & GetBaseName((newFile.FileName)) & ": no destination folder found **")
            PostMessage = 333
            Exit Function
        End If

        'create exchange message(s)
        folderCount = 0
        'loop through the folder pointers
        For Each thisFolder In coDestFolders
            'Make a copy if the message is already created
            If folderCount > 0 Then
                cpyMessage = orgMessage.CopyTo(thisFolder.ID)
                cpyMessage.Update()
                Logger.Log(LOGFILE, "PostMessage -> ", newFile.FipHeaders.Item(strMessageTypeHeader).Content & " -> " & thisFolder.Name)

            Else
                orgMessage = thisFolder.Messages.Add
                orgMessage.Sender = mpsSession.CurrentUser

                On Error Resume Next
                'Find message type (ie. IPM.Note.MyType), if specified
                thisVal = newFile.FipHeaders.Item(strMessageTypeHeader).Content

                If thisVal <> "" Then
                    orgMessage.Type = strMessageTypeRoot & "." & thisVal
                    Logger.Log(LOGFILE, "PostMessage !i", "Message type: " & orgMessage.Type)
                Else
                    'set to default
                    orgMessage.Type = strDefaultMessageType
                End If
                ''Logger.Log LOGFILE, "PostMessage !d", "Message type: " & orgMessage.Type

                'Set subject
                orgMessage.Subject = newFile.FipHeaders.Item(strSubjectHeader).Content
                orgMessage.Unread = True
                orgMessage.Update()

                ' Henter ut RTF Tekst av meldingen 
                On Error GoTo post_err

                Dim SafeItem As Redemption.SafeMailItem
                SafeItem = CreateObject("Redemption.SafeMailItem")
                SafeItem.Item = orgMessage
                SafeItem.RTFBody = (strRTFtekst)
                orgMessage = SafeItem.Item
                orgMessage.Type = strMessageTypeRoot
                orgMessage.Unread = True
                orgMessage.Update()

                msgID = orgMessage.ID 'save message id

                're-open
                orgMessage = mpsSession.GetMessage(msgID, System.DBNull.Value) 'get modified message
                '  End If

                'check for priority flag
                On Error Resume Next
                thisVal = newFile.FipHeaders.Item(strPriFlagHeader).Content
                Select Case LCase(thisVal)
                    Case "high"
                        orgMessage.Importance = MAPI.CdoImportance.CdoHigh
                    Case "low"
                        orgMessage.Importance = MAPI.CdoImportance.CdoLow
                End Select

                'add user defined fields to message
                msgFields = orgMessage.Fields

                For Each formfld In coFormFields
                    If formfld.FormField <> "" Then
                        thisVal = ""
                        On Error Resume Next
                        thisVal = newFile.FipHeaders.Item(formfld.FipHeader).Content
                        If formfld.FormField = "NTBDistribusjonsKode" And thisVal = "" Then
                            thisVal = "All"
                        End If
                        If thisVal <> "" Then
                            aMessageField = msgFields.Add(formfld.FormField, formfld.FieldType, thisVal)
                        End If
                        thisVal = ""
                    End If
                Next formfld

                formfld = Nothing

                'commit changes
                orgMessage.Unread = True
                orgMessage.Update()

                Logger.Log(LOGFILE, "ExchangeMessage -> ", orgMessage.Subject & " -> " & thisFolder.Name)
                folderCount = folderCount + 1
            End If
        Next thisFolder

        'Rydder opp!
        thisFolder = Nothing
        coDestFolders = Nothing
        msgFields = Nothing
        cpyMessage = Nothing
        orgMessage = Nothing

        strRTFtekst = ""

        PostMessage = 0

        Exit Function

post_err:
        Logger.Log(LOGFILE, "PostMessage !x", "** Error posting " & newFile.FileName & ": " & Err.Description)
        fRoot = Nothing
        PostMessage = 1
    End Function


    Private Function GetSession() As Short

        LogonExchange()

    End Function


    Public Sub LogonExchange()
        Dim strPublicRootID As String

        Const PR_IPM_PUBLIC_FOLDERS_ENTRYID = &H66310102

        'Create new session
        mpsSession = New MAPI.SessionClass

        'Check profile settings an log on
        Try
            If strProfileName = "" Then
                mpsSession.Logon(, , False, False, 0, True)
            Else
                mpsSession.Logon(strProfileName, , False, True, 0, True)
            End If
            Logger.Log(LOGFILE, "Logged on to " & mpsSession.Name, "----")
        Catch ex As Exception
            Logger.Log(LOGFILE, "MS Exchange logon failed.", ex.ToString)
        End Try

        'Get infostore and root folder
        Try
            'Language independent public folder fetch
            For Each isInfoStore In mpsSession.InfoStores
                Try
                    strPublicRootID = isInfoStore.Fields(PR_IPM_PUBLIC_FOLDERS_ENTRYID).Value()

                    ' Get root folder
                    fRoot = mpsSession.GetFolder(strPublicRootID, isInfoStore.ID)
                    Exit For
                Catch ex As Exception
                    Logger.Log(LOGFILE, "Root folder loop exception: " & isInfoStore.Name, ex.ToString)
                End Try
            Next

            Logger.Log(LOGFILE, "Using InfoStore: " & isInfoStore.Name, "----")
            Logger.Log(LOGFILE, "Using root folder: " & fRoot.Name, "----")
        Catch ex As Exception
            Logger.Log(LOGFILE, "Failed to retrieve infostore and root folder object.", ex.ToString)
        End Try

    End Sub



    'Gets infostore object by name
    Private Function GetInfoStoreByName(ByVal strInfoStoreName As String) As MAPI.InfoStore

        Dim store As MAPI.InfoStore
        Dim stores As MAPI.InfoStores = mpsSession.InfoStores

        Dim i As Integer
        For i = 1 To stores.Count
            store = stores.Item(i)
            If store.Name = strInfoStoreName Then Return store
        Next

        'Return nothing, i.e not found
        GetInfoStoreByName = Nothing
    End Function








    Private Function GetFolderByName(ByVal strFolderName As String, ByVal colFolders As MAPI.Folders) As MAPI.Folder

        'Input:     RootFolder, Foldername, Folder collection
        'Output:    Folder object

        Dim F As MAPI.Folder 'For each folder...
        Dim myFolders As Object
        Dim TempF As MAPI.Folder 'Intermediate foldername
        Dim strRemFolderName As String 'Remaining part of folder path
        Dim strSingleFolderName As String 'Single foldername
        Dim tmpString As String
        Dim i As Short
        Dim ourFolders As MAPI.InfoStore

        strRemFolderName = strFolderName

        Do
            i = strRemFolderName.IndexOf("\")
            If i >= 0 Then
                strSingleFolderName = strRemFolderName.Substring(0, i)
            Else
                strSingleFolderName = strRemFolderName
            End If

            'Loop through folders
            Dim bFound As Boolean
            bFound = False
            F = colFolders.GetFirst

            Do While (Not bFound)
                tmpString = F.Name
                If tmpString.ToLower() = strSingleFolderName.ToLower() Then
                    TempF = F
                    'Limiting folders collection to subfolders of current folder
                    colFolders = TempF.Folders
                    bFound = True
                Else
                    F = colFolders.GetNext()
                    'MsgBox(F.Name)
                    bFound = False
                End If
            Loop

            If i >= 0 Then
                strRemFolderName = strRemFolderName.Substring(i + 1)
                bFound = False
            Else
                Return TempF
            End If

        Loop Until i = -1

        'If program reaches here, folder was not found: return Nothing
        MsgBox("Folder not found.", CDbl("Error"))
        Err.Raise(vbObjectError + 12, "Form.GetFolderByName", "Folder not found: " & strRemFolderName & ".")

        Return Nothing
    End Function




    Private Function ExtractFolderList(ByRef strFolderName As String, ByRef fRoot As MAPI.Folder) As Collection


        Dim strSubFolder As String
        Dim colFolderCollection As Collection
        Dim objFolderObject As cFolderObject
        Dim i As Object
        Dim j As Short
        Dim l As Short
        Dim test As MAPI.Folder

        colFolderCollection = New Collection

        'UPGRADE_WARNING: Couldn't resolve default property of object i. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
        i = 1
        j = InStr(1, strFolderName, ":")
        Do While j > 0
            'UPGRADE_WARNING: Couldn't resolve default property of object i. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
            strSubFolder = Mid(strFolderName, i, j - i)
            test = GetFolderObject(strSubFolder, fRoot)
            If Not test Is Nothing Then
                colFolderCollection.Add(test, strSubFolder)
            End If

            strSubFolder = ""
            'UPGRADE_WARNING: Couldn't resolve default property of object i. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
            i = j + 1
            'UPGRADE_WARNING: Couldn't resolve default property of object i. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
            j = InStr(i, strFolderName, ":")
        Loop

        'UPGRADE_WARNING: Couldn't resolve default property of object i. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1037"'
        strSubFolder = VB.Right(strFolderName, Len(strFolderName) - i + 1)
        test = GetFolderObject(strSubFolder, fRoot)
        If Not test Is Nothing Then
            colFolderCollection.Add(test, strSubFolder)
        End If

        If colFolderCollection Is Nothing Then
            Logger.Log(LOGFILE, "ExtractFolderList !x", "** No folders found: " & strFolderName)
        End If

        ExtractFolderList = colFolderCollection

    End Function

    Private Function GetFolderObject(ByRef strFolderPath As String, ByRef fRoot As MAPI.Folder) As MAPI.Folder
        Dim thisFolder As cFolderObject
        Dim objFolder As MAPI.Folder
        Dim strFolder As String

        strFolder = LCase(strFolderPath)

        For Each thisFolder In colFolderObjects
            If thisFolder.Path = strFolder Then
                objFolder = thisFolder.Folder
                Exit For
            End If
        Next thisFolder

        If objFolder Is Nothing Then
            objFolder = GetFolderByName(strFolder, (fRoot.Folders))
            If Not objFolder Is Nothing Then
                Logger.Log(LOGFILE, "GetFolderObject !d", "Appending folder " & strFolder & " to folder collection")
                On Error Resume Next
                thisFolder = New cFolderObject
                thisFolder.Folder = objFolder
                thisFolder.Path = strFolder
                colFolderObjects.Add(thisFolder)
                'UPGRADE_NOTE: Object thisFolder may not be destroyed until it is garbage collected. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1029"'
                thisFolder = Nothing
                On Error GoTo 0
            End If
        End If

        GetFolderObject = objFolder
    End Function

    Private Function GetBaseName(ByRef fullName As String) As String
        'Gets the filename part from a full path

        Dim i As Short

        i = InStrRev(fullName, "\")
        If i > 0 Then
            GetBaseName = VB.Right(fullName, Len(fullName) - i)
        Else
            GetBaseName = fullName
        End If

    End Function

    Protected Overrides Sub Finalize()

        MyBase.Finalize()
    End Sub


End Class