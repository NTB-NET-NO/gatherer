Option Strict Off
Option Explicit On
Friend Class cErrorFile
	Private mvarFileName As String
	Private mvarCount As Short
	Private mvarLastTry As Date
	
	
	Public Property FileName() As String
		Get
			FileName = mvarFileName
		End Get
		Set(ByVal Value As String)
			mvarFileName = Value
		End Set
	End Property
	
	
	Public Property Count() As Short
		Get
			Count = mvarCount
		End Get
		Set(ByVal Value As Short)
			mvarCount = Value
		End Set
	End Property
	
	
	Public Property LastTry() As Date
		Get
			LastTry = mvarLastTry
		End Get
		Set(ByVal Value As Date)
			mvarLastTry = Value
		End Set
	End Property
	
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
	Private Sub Class_Initialize_Renamed()
		mvarCount = 0
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
End Class