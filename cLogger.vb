Option Strict Off
Option Explicit On
Friend Class cLogger
	
	'local variable(s) to hold property value(s)
	Private mvarFileName As String 'local copy
	Private mvarMessage As String 'local copy
	Private mvarSource As String 'local copy
	Public Sub Log(Optional ByRef strFilename As String = "", Optional ByRef strSource As String = "", Optional ByRef strMessage As String = "")
		
		Dim filehandle As Short
		
		If strFilename <> "" Then Me.FileName = strFilename
		If strSource <> "" Then Me.Source = strSource
		If strMessage <> "" Then Me.Message = strMessage
		
		filehandle = FreeFile
		
		On Error Resume Next
		If mvarFileName = "" Then mvarFileName = "logfile.log"
		FileOpen(filehandle, mvarFileName, OpenMode.Append)
		On Error GoTo 0
		
		PrintLine(filehandle, Now & " " & mvarSource & " : " & mvarMessage)
		
		FileClose(filehandle)
		
	End Sub
	
	Public Sub Init(ByRef strFilename As String, ByRef strSource As String, ByRef strMessage As Object)
		
	End Sub
	
	
	Public Property Source() As String
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Source
			Source = mvarSource
		End Get
		Set(ByVal Value As String)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Source = 5
			mvarSource = Value
		End Set
	End Property
	
	
	Public Property Message() As String
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Message
			Message = mvarMessage
		End Get
		Set(ByVal Value As String)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Message = 5
			mvarMessage = Value
		End Set
	End Property
	
	
	Public Property FileName() As String
		Get
			'used when retrieving value of a property, on the right side of an assignment.
			'Syntax: Debug.Print X.Filename
			FileName = mvarFileName
		End Get
		Set(ByVal Value As String)
			'used when assigning a value to the property, on the left side of an assignment.
			'Syntax: X.Filename = 5
			mvarFileName = Value
		End Set
	End Property
End Class