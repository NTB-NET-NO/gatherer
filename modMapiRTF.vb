Option Strict Off
Option Explicit On
Module modMapiRTF
	' For the Debug version of the DLL, remark the following line
    Public Declare Function WriteRTF Lib "mapirtf.dll" Alias "writertf" (ByVal ProfileName As String, ByVal MessageID As String, ByVal StoreID As String, ByVal cText As String) As Short
    '	Public Declare Function ReadRTF Lib "mapirtf.dll"  Alias "readrtf"(ByVal ProfileName As String, ByVal SrcMsgID As String, ByVal SrcStoreID As String, ByRef MsgRTF As String) As Short
	
	' For the Debug version of the DLL, un-remark the following line
    'Public Declare Function WriteRTF _
    'Lib "mapirtfd.dll" _
    'Alias "writertf" (ByVal ProfileName As String, ByVal MessageID As String, _
    'ByVal StoreID As String, ByVal cText As String) _
    'As Integer
    'Public Declare Function ReadRTF _
    'Lib "mapirtfd.dll" _
    'Alias "readrtf" (ByVal ProfileName As String, ByVal SrcMsgID As String, _
    'ByVal SrcStoreID As String, ByRef MsgRTF As String) _
    'As Integer
End Module