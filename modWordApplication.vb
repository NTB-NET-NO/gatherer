Option Strict Off
Option Explicit On
Module modWordApplication
	
	'-----------------------------------------------------------------
	'Description:
	'   tries to aquire a link to an existing Word application.
	'   if none is found, it creates a new instance of Word.
	'-----------------------------------------------------------------
	Public Function gGetWordApplication() As Microsoft.Office.Interop.Word.Application
		Dim wdApp As Microsoft.Office.Interop.Word.Application
		On Error Resume Next
		wdApp = GetObject( , "Word.Application")
		'Set wdApp = CreateObject("Word.Application")
		If Err.Number <> 0 Then
			wdApp = New Microsoft.Office.Interop.Word.Application
		End If
		On Error GoTo 0
		gGetWordApplication = wdApp
	End Function
End Module