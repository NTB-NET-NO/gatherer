Option Strict Off
Option Explicit On
Module ModINIFileHandling
    'Date:          2005.09
	'Description:   This module declares function(s) to read values from an ini file.
    'Author:        Terje Kristiansen
	
	
	'Library functions from kernel32
	'UPGRADE_ISSUE: Declaring a parameter 'As Any' is not supported. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1016"'
    Public Declare Function gfnGetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
	Public Declare Function gfnGetPrivateProfileInt Lib "kernel32"  Alias "GetPrivateProfileIntA"(ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal nDefault As Integer, ByVal lpFileName As String) As Integer
	
	Function gfnGetFromIni(ByRef iniFile As String, ByRef strSection As String, ByRef strKey As String, ByRef strDefaultValue As String) As String
		
		'Date:          1999.05.21
		'Description:   This function is a wrapper for the "GetPrivateProfileStringA" function.
		'Author:        John Henrik Andersen, minor modifications by Svein Terje Gaup
		'Input:         Section of ini file, Key in ini file, Default value if key is not found or key value is empty
		'Output:        Value from ini file
		'Uses:          GetPrivateProfileStringA from kernel32
		
		Dim intI As Integer
		Dim strReturnString As String 'String buffer
		
		'Fills string buffer with null-character (Ascii char number 0)
		strReturnString = New String(Chr(0), 150)
		
		'Gets value from ini file
		gfnGetPrivateProfileString(strSection, strKey, strDefaultValue, strReturnString, Len(strReturnString) + 1, iniFile)
		
		'Removes null-characters from end of string
		For intI = 0 To Len(strReturnString) + 1
			If Asc(Right(Left(strReturnString, intI + 1), 1)) = 0 Then
				Exit For
			End If
		Next 
		
		'Returns String without null characters
		gfnGetFromIni = Left(strReturnString, intI)
		
	End Function
End Module