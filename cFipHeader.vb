Option Strict Off
Option Explicit On
Friend Class cFipHeader
	
	Private mvarHeaderName As String
	Private mvarContent As String
	
	
	Public Property HeaderName() As String
		Get
			HeaderName = mvarHeaderName
		End Get
		Set(ByVal Value As String)
			mvarHeaderName = Value
		End Set
	End Property
	
	
	Public Property Content() As String
		Get
			Content = mvarContent
		End Get
		Set(ByVal Value As String)
			mvarContent = Value
		End Set
	End Property
End Class