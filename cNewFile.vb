Option Strict Off
Option Explicit On
Friend Class cNewFile
	
	Private mvarPlainText As Boolean
	Private mvarFileName As String
	Private mvarBodyText As String
	Private mvarFipHeaders As Collection
	
	
	Public Property PlainText() As Boolean
		Get
			PlainText = mvarPlainText
		End Get
		Set(ByVal Value As Boolean)
			mvarPlainText = Value
		End Set
	End Property
	
	
	Public Property FileName() As String
		Get
			FileName = mvarFileName
		End Get
		Set(ByVal Value As String)
			mvarFileName = Value
		End Set
	End Property
	
	
	Public Property BodyText() As String
		Get
			BodyText = mvarBodyText
		End Get
		Set(ByVal Value As String)
			mvarBodyText = Value
		End Set
	End Property
	
	
	Public Property FipHeaders() As Collection
		Get
			FipHeaders = mvarFipHeaders
		End Get
		Set(ByVal Value As Collection)
			mvarFipHeaders = Value
		End Set
	End Property
	
	'UPGRADE_NOTE: Class_Initialize was upgraded to Class_Initialize_Renamed. Click for more: 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="vbup1061"'
	Private Sub Class_Initialize_Renamed()
		mvarFipHeaders = New Collection
		mvarPlainText = False
	End Sub
	Public Sub New()
		MyBase.New()
		Class_Initialize_Renamed()
	End Sub
	
	Private Sub Class_Terminate_Renamed()
        mvarFipHeaders = Nothing
	End Sub
	Protected Overrides Sub Finalize()
		Class_Terminate_Renamed()
		MyBase.Finalize()
	End Sub
End Class