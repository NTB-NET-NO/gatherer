Option Strict Off
Option Explicit On
Friend Class cFormField
	
	Private mvarFormField As String
	Private mvarFipHeader As String
	Private mvarFieldType As Short
	
	
	Public Property FormField() As String
		Get
			FormField = mvarFormField
		End Get
		Set(ByVal Value As String)
			mvarFormField = Value
		End Set
	End Property
	
	
	Public Property FipHeader() As String
		Get
			FipHeader = mvarFipHeader
		End Get
		Set(ByVal Value As String)
			mvarFipHeader = Value
		End Set
	End Property
	
	
	Public Property FieldType() As Short
		Get
			FieldType = mvarFieldType
		End Get
		Set(ByVal Value As Short)
			mvarFieldType = Value
		End Set
	End Property
End Class