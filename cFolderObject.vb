Option Strict Off
Option Explicit On
Friend Class cFolderObject
	
	Private mvarFolder As MAPI.Folder
	Private mvarPath As String
	
	
	
	
	Public Property Folder() As MAPI.Folder
		Get
			Folder = mvarFolder
		End Get
		Set(ByVal Value As MAPI.Folder)
			mvarFolder = Value
		End Set
	End Property
	
	
	Public Property Path() As String
		Get
			Path = mvarPath
		End Get
		Set(ByVal Value As String)
			mvarPath = Value
		End Set
	End Property
End Class